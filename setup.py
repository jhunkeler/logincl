from setuptools import setup, find_packages

setup(
      name="logincl",
      version='1.0.0',
      packages=find_packages(),
      package_data={
                    '': ['LICENSE.txt']
                    },
      author='Joseph Hunkeler',
      author_email='jhunk@stsci.edu',
      description='Solves the login.cl in every directory problem.',
      license='BSD',
      keywords='iraf pyraf login.cl',
      url='https://bitbucket.org/jhunkeler/logincl'
)
