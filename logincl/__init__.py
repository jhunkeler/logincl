import os
import atexit


DEFAULT_CLFILE = os.path.join(os.environ['HOME'], 'iraf', 'login.cl')

if 'LOGINCL' in os.environ:
    CLFILE = os.environ['LOGINCL']
else:
    CLFILE = DEFAULT_CLFILE

class LoginCLError(Exception):
    pass

class LoginCL(object):
    def __init__(self, filename):
        self.filename = os.path.abspath(filename)

        if not os.path.exists(self.filename):
            raise LoginCLError('{0} does not exist.'.format(self.filename))

        if not os.path.isfile(self.filename):
            raise LoginCLError('{0} is not a file.'.format(self.filename))

        self.curdir = os.path.abspath(os.curdir)
        self.destination = os.path.join(self.curdir, os.path.basename(self.filename))

        if self.exists():
            if not self.rmlink():
                return
        self.mklink()

    def exists(self):
        return os.path.exists(self.destination)

    def mklink(self):
        os.symlink(self.filename, self.destination)

    def rmlink(self):
        if not os.path.islink(self.destination):
            return False

        os.remove(self.destination)
        return True

try:
    lcl = LoginCL(CLFILE)
    atexit.register(lcl.rmlink)
except LoginCLError:
    pass
